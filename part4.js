//Name = Johan Vermaak;
//Date = 29/03/2018;
//ID = 10015918;

let input = "";

input = prompt("Enter a lett of the alphabet to determine if it is a vowel or a consonant").toUpperCase(); //forces the input to be uppercase

switch(input){
    case "A": 
        console.log(`${input} is a vowel`);         //when input = 'A' it will display it as a vowel
        break;
    case "B":
    case "C":
    case "D": 
        console.log(`${input} is a consonant`);     //anything between B and D will give an output of consonant
        break;
    case "E": 
        console.log(`${input} is a vowel`);         //when input = 'E' it will display it as a vowel
        break;
    case "F":
    case "G":
    case "H": 
        console.log(`${input} is a consonant`);     //anything between F and H will give an output of consonant
        break;
    case "I": 
        console.log(`${input} is a vowel`);         //when input = 'I' it will display it as a vowel
        break;
    case "J":
    case "K":
    case "L":
    case "M":
    case "N": 
        console.log(`${input} is a consonant`);     //anything between J and N will give an output of consonant
        break;
    case "O": 
        console.log(`${input} is a vowel`);         //when input = 'O' it will display it as a vowel
        break;
    case "P":
    case "Q":
    case "R":
    case "S":
    case "T": 
        console.log(`${input} is a consonant`);     //anything between P and T will give an output of consonant
        break;
    case "U": 
        console.log(`${input} is a vowel`);         //when input = 'U' it will display it as a vowel
        break;
    case "V":
    case "W":
    case "X":
    case "Y":
    case "Z":
        console.log(`${input} is a consonant`);     //anything between V and Z will give an output of consonant
        break;
}