//Name = Johan Vermaak;
//Date = 29/03/2018;
//ID = 10015918;

let customerID = 0;                                                                         //variable for customer ID
let name = "";                                                                              // variable for customer name
let unitsConsumed = 0;                                                                      // variable for the amount of units customer used
let total = 0;                                                                              // variable to use as total for units used * charge per unit

customerID = prompt("Please enter the customer's ID number");                               //input for user ID number
name = prompt("Please enter the customer's name");                                          //input for user/customer name
unitsConsumed = prompt(`Please input the amount of units ${name} has used`);                //input for amount of units used

if(unitsConsumed < 200){                                                                    // for units below 200 but not including 200 will run this section of code
    console.log(`ID:    ${customerID}\nName:    ${name}\nUnits:     ${unitsConsumed}`);     //output for ID, Name, and units consumed
    total = Number(unitsConsumed*1.20).toFixed(2);                                          // units used by customer * charge for under 200 units
    console.log(`electricty bill owing @ $1.20 per unit is: $${total}`);                    //output of total charge for units consumed
}
else if(unitsConsumed >= 200 && unitsConsumed < 400){                                       //for units between 200 and 399 will run this section of code
    console.log(`ID:    ${customerID}\nName:    ${name}\nUnits:     ${unitsConsumed}`);     //output for ID, Name, and units consumed
    total = Number(unitsConsumed*1.50).toFixed(2);                                          //units used by customer * charge for between 200 and 399 units
    console.log(`electricty bill owing @ $1.50 per unit is: $${total}`);                    //output of total charge for units consumed
}
else if(unitsConsumed >= 400 && unitsConsumed < 600){                                       //for units between 400 and 599 will run this section of code
    console.log(`ID:    ${customerID}\nName:    ${name}\nUnits:     ${unitsConsumed}`);     //output for ID, Name, and units consumed
    total = Number(unitsConsumed*1.80).toFixed(2);                               //units used by customer * charge for between 400 and 599 units
    console.log(`electricty bill owing @ $1.80 per unit is: $${total}`);                    //output of total charge for units consumed
}
else{                                                                                       //will run this section of code for everything not specified like above
    console.log(`ID:    ${customerID}\nName:    ${name}\nUnits:     ${unitsConsumed}`);     //output for ID, Name, and units consumed
    total = Number(unitsConsumed*2.00).toFixed(2);                               //charge for 600+ as this code should only run for that amount of units
    console.log(`electricty bill owing @ $2.00 per unit is: $${total}`);                    //output of total charge for units consumed
    }