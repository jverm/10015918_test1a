//Name = Johan Vermaak;
//Date = 29/03/2018;
//ID = 10015918;

let number = 1; //number of times loop is repeated
let input = 0; //input for each time it loops
let total = 0; // to append/add all the inputs

for(number = 1; number <= 10; number++){ //this loop will run from number 1 to number 10
    input = prompt(`Please enter number ${number}`); //promt for users number
    console.log(`Number ${number}: ${input}`); //to display each number that has been inputed
    total = Number(total) + Number(input); //appends/adds all the numbers together 
}
total = total/(number-1); //divides the total by 10 note: the minus 1 is there because it was dividing by 11
console.log(`The average of those 10 numbers is ${total}`); //displays the average