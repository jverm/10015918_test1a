//Name = Johan Vermaak;
//Date = 29/03/2018;
//ID = 10015918;

let celcius = 0; //variable for celcius input
let fahrenheit = 0; //variable for fahrenheit output

celcius = prompt("Please input a temperature in degrees Celcius to be converted"); //prompting user for their input in degrees celcius
fahrenheit = (celcius*18/10)+32; //the formula to convert celcius to fahrenheit
console.log(`${celcius} degrees celcius is equal to ${fahrenheit} degrees fahrenheit`); //the output with the converted degrees to fahrenheit